<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @package   Modules
 * @category  Imagefly
 * @author    Fady Khalife
 * @uses      Image Module
 */

return array
(
    'cache_expire'     => 7 * 24 * 60 * 60,
    'cache_dir'        => DOCROOT.'asset/img/',
    'mimic_source_dir' => TRUE,
    'enforce_presets'  => FALSE,
	'scale_up'		   => FALSE,
    'presets'          => array(
        /*'thumbnail' => array(
            'watermark' => array(
                'image'     => 'path_image',
                'offset_x'  => 10,
                'offset_y'  => 10
            ),
            'w' => 170,
            'c' => FALSE,
            's' => 25,
            'q' => 80,
            'g' => TRUE,
        ),*/
    ),
);

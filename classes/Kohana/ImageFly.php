<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * @package   Modules
 * @category  Imagefly
 * @author    Fady Khalife
 * @uses      Image Module
 */
class Kohana_ImageFly {

    /**
     * @var  array       This modules config options
     */
    protected $config = NULL;

    /**
     * @var  string      Stores the path to the cache directory which is either whats set in the config "cache_dir"
     *                   or processed sub directories when the "mimic_source_dir" config option id set to TRUE
     */
    protected $cache_dir = NULL;

    /**
     * @var  Image      Kohana image instance
     */
    protected $image = NULL;

    /**
     * @var  boolean     A flag for weither we should serve the default or cached image
     */
    protected $serve_default = FALSE;

    /**
     * @var  string      The source filepath and filename
     */
    protected $source_file = NULL;

    /**
     * @var  array       Stores the URL params in the following format
     *                   w = Width (int)
     *                   h = Height (int)
     *                   c = Crop (bool)
     *                   q = Quality (int)
     */
    protected $url_params = array();

    /**
     * @var  string      Last modified Unix timestamp of the source file
     */
    protected $source_modified = NULL;

    /**
     * @var  string      The cached filename with path ($this->cache_dir)
     */
    protected $cached_file = NULL;

    /**
     * @var string
     */
    protected $_request_params;

    /**
     * @var string
     */
    protected $_image_path;

    /**
     * Constructorbot
     */
    public function __construct($request_params, $image_path) {
        $this->_request_params = $request_params;
        $this->_image_path = $image_path;

        // Prevent unnecessary warnings on servers that are set to display E_STRICT errors, these will damage the image data.
        error_reporting(error_reporting() & ~E_STRICT);

        // Set the config
        $this->config = Kohana::$config->load('imagefly');

        // Try to create the cache directory if it does not exist
        $this->_create_cache_dir();

        // Parse and set the image modify params
        $this->_set_params();

        // Set the source file modified timestamp
        $this->source_modified = filemtime($this->source_file);

        // Try to create the mimic directory structure if required
        $this->_create_mimic_cache_dir();

        // Set the cached filepath with filename
        $this->cached_file = $this->cache_dir . pathinfo($this->source_file, PATHINFO_BASENAME);

        // Create a modified cache file or dont...
        if (!$this->_cached_exists()) {
            $this->_create_cached();
        }

        // Serve the image file
        $this->_serve_file();
    }

    /**
     * Try to create the config cache dir if required
     * Set $cache_dir
     */
    protected function _create_cache_dir() {
        if (!file_exists($this->config['cache_dir'])) {
            try {
                mkdir($this->config['cache_dir'], 0755, TRUE);
            } catch (Exception $e) {
                throw new Kohana_Exception($e);
            }
        }

        // Set the cache dir
        $this->cache_dir = $this->config['cache_dir'];
    }

    /**
     * Try to create the mimic cache dir from the source path if required
     * Set $cache_dir
     */
    protected function _create_mimic_cache_dir() {
        if ($this->config['mimic_source_dir']) {
            // Get the dir from the source file
            $mimic_dir = $this->config['cache_dir']
                    . Request::current()->param('params') . DIRECTORY_SEPARATOR
                    . pathinfo($this->source_file, PATHINFO_DIRNAME);

            // Try to create if it does not exist
            if (!file_exists($mimic_dir)) {
                try {
                    mkdir($mimic_dir, 0755, TRUE);
                } catch (Exception $e) {
                    throw new Kohana_Exception($e);
                }
            }

            // Set the cache dir, with trailling slash
            $this->cache_dir = $mimic_dir . '/';
        }
    }

    /**
     * Sets the operations params from the url
     * w = Width (int)
     * h = Height (int)
     * c = Crop (bool)
     */
    protected function _set_params() {
        // Get values from request
        $params = $this->_request_params;

        // If enforcing params, ensure it's a match
        if ($this->config['enforce_presets'] AND
                !isset($this->config['presets'][$params])
        ) {
            throw new HTTP_Exception_404('The requested URL :uri was not found on this server.', array(':uri' => Request::$current->uri()));
        }

        // Initialize image
        $this->image = Image::factory($this->_image_path);

        if (isset($this->config['presets'][$params])) {
            $this->_set_preset_params();
        } else {
            $this->_set_route_params();
        }

        // Set the url filepath
        $this->source_file = $this->_image_path;
    }

    protected function _set_preset_params() {
        $this->url_params = $this->config['presets'][$this->_request_params];

        // When croping, we must have a width and height to pass to imagecreatetruecolor method
        // Make width the height or vice versa if either is not passed
        if (empty($this->url_params['w']) && !empty($this->url_params['h'])) {
            $this->url_params['w'] = $this->url_params['h'];
        }
        if (empty($this->url_params['h']) && !empty($this->url_params['w'])) {
            $this->url_params['h'] = $this->url_params['w'];
        }

        //Do not scale up images
        if (!$this->config['scale_up']) {
            if (!empty($this->url_params['w']) && $this->url_params['w'] > $this->image->width)
                $this->url_params['w'] = $this->image->width;
            if (!empty($this->url_params['h']) && $this->url_params['h'] > $this->image->height)
                $this->url_params['h'] = $this->image->height;
        }
    }

    protected function _set_route_params() {
        // The parameters are separated by hyphens
        $raw_params = explode('-', $this->_request_params);

        // Set default param values
        $this->url_params['w'] = NULL;
        $this->url_params['h'] = NULL;
        $this->url_params['c'] = FALSE;
        $this->url_params['q'] = NULL;
        $this->url_params['s'] = NULL;
        $this->url_params['g'] = NULL;

        // Update param values from passed values
        foreach ($raw_params as $raw_param) {
            $name = $raw_param[0];
            $value = substr($raw_param, 1, strlen($raw_param) - 1);
            if ($name == 'c') {
                $this->url_params[$name] = TRUE;

                // When croping, we must have a width and height to pass to imagecreatetruecolor method
                // Make width the height or vice versa if either is not passed
                if (empty($this->url_params['w'])) {
                    $this->url_params['w'] = $this->url_params['h'];
                }
                if (empty($this->url_params['h'])) {
                    $this->url_params['h'] = $this->url_params['w'];
                }
            }elseif ($name == 'g') {
                $this->url_params[$name] = TRUE;
            }else {
                $this->url_params[$name] = $value;
            }
        }

        //Do not scale up images
        if (!$this->config['scale_up']) {
            if ($this->url_params['w'] > $this->image->width)
                $this->url_params['w'] = $this->image->width;
            if ($this->url_params['h'] > $this->image->height)
                $this->url_params['h'] = $this->image->height;
        }
    }

    /**
     * Checks if a physical version of the cached image exists
     *
     * @return boolean
     */
    protected function _cached_exists() {
        return file_exists($this->cached_file);
    }

    /**
     * Creates a cached cropped/resized version of the file
     */
    protected function _create_cached()
    {
        if ( ! empty($this->url_params['c'])) {
            // Resize to highest width or height with overflow on the larger side
            $this->image->resize($this->url_params['w'], $this->url_params['h'], Image::INVERSE);

            // Crop any overflow from the larger side
            $this->image->crop($this->url_params['w'], $this->url_params['h']);
        } else {
            // Just Resize
            $this->image->resize($this->url_params['w'], $this->url_params['h']);
        }

        if ( ! empty($this->url_params['watermark'])) {
            $watermark = $this->url_params['watermark'];
            $this->image->watermark(
                    Image::factory($watermark['image']),
                    !empty($watermark['offset_x']) ? $watermark['offset_x'] : NULL,
                    !empty($watermark['offset_y']) ? $watermark['offset_y'] : NULL,
                    !empty($watermark['opacity']) ? $watermark['opacity'] : 100
            );
        }

        // sharpen
        if ( ! empty($this->url_params['s'])) {
            $this->image->sharpen($this->url_params['s']);
        }

		// gray
        if ( ! empty($this->url_params['g'])) {
            $this->image->grayscale();
        }

        // Save
        if ( ! empty($this->url_params['q'])) {
            //Save image with quality param
            $this->image->save($this->cached_file, $this->url_params['q']);
        } else {
            //Save image with default quality
            $this->image->save($this->cached_file);
        }
    }

    /**
     * Create the image HTTP headers
     *
     * @param  string     path to the file to server (either default or cached version)
     */
    protected function _create_headers($file_data) {
        // Create the required header vars
        $last_modified = gmdate('D, d M Y H:i:s', filemtime($file_data)) . ' GMT';
        $content_type = File::mime($file_data);
        $content_length = filesize($file_data);
        $expires = gmdate('D, d M Y H:i:s', (time() + $this->config['cache_expire'])) . ' GMT';
        $max_age = 'max-age=' . $this->config['cache_expire'] . ', public';

        // Some required headers
        header("Last-Modified: $last_modified");
        header("Content-Type: $content_type");
        header("Content-Length: $content_length");

        // How long to hold in the browser cache
        header("Expires: $expires");

        /**
         * Public in the Cache-Control lets proxies know that it is okay to
         * cache this content. If this is being served over HTTPS, there may be
         * sensitive content and therefore should probably not be cached by
         * proxy servers.
         */
        header("Cache-Control: $max_age");

        // Set the 304 Not Modified if required
        $this->_modified_headers($last_modified);

        /**
         * The "Connection: close" header allows us to serve the file and let
         * the browser finish processing the script so we can do extra work
         * without making the user wait. This header must come last or the file
         * size will not properly work for images in the browser's cache
         */
        header("Connection: close");
    }

    /**
     * Rerurns 304 Not Modified HTTP headers if required and exits
     *
     * @param  string  header formatted date
     */
    protected function _modified_headers($last_modified) {
        $modified_since = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) ? stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) : FALSE;

        if (!$modified_since OR $modified_since != $last_modified)
            return;

        // Nothing has changed since their last request - serve a 304 and exit
        header('HTTP/1.1 304 Not Modified');
        header('Connection: close');
        exit();
    }

    /**
     * Decide which filesource we are using and serve
     */
    protected function _serve_file() {
        // Set either the source or cache file as our datasource
        if ($this->serve_default) {
            $file_data = $this->source_file;
        } else {
            $file_data = $this->cached_file;
        }

        // Output the file
        $this->_output_file($file_data);
    }

    /**
     * Outputs the cached image file and exits
     *
     * @param  string     path to the file to server (either default or cached version)
     */
    protected function _output_file($file_data) {
        // Create the headers
        $this->_create_headers($file_data);

        // Get the file data
        $data = file_get_contents($file_data);

        // Send the image to the browser in bite-sized chunks
        $chunk_size = 1024 * 8;
        $fp = fopen('php://memory', 'r+b');

        // Process file data
        fwrite($fp, $data);
        rewind($fp);
        while (!feof($fp)) {
            echo fread($fp, $chunk_size);
            flush();
        }
        fclose($fp);

        exit();
    }

}

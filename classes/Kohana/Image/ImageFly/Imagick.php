<?php

class Kohana_Image_ImageFly_Imagick extends Image_Imagick {

    public function grayscale()
    {
        $this->im->setimagetype(Imagick::IMGTYPE_GRAYSCALE);

        return $this;
    }

}
<?php

class Kohana_Image_ImageFly_GD extends Image_GD {

    public function grayscale()
    {
        // Loads image if not yet loaded
        $this->_load_image();

        imagefilter($this->_image, IMG_FILTER_GRAYSCALE);

        return $this;
    }

}
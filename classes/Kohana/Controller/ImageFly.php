<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @package   Modules
 * @category  Imagefly
 * @author    Fady Khalife
 * @uses      Image Module
 */

class Kohana_Controller_ImageFly extends Controller
{
    public function action_index()
    {
    	if (version_compare(phpversion('imagick'), '2.0.1', '>='))
        {
            Image::$default_driver = 'ImageFly_Imagick';
        }
        else
        {
            Image::$default_driver = 'ImageFly_GD';
        }

        new ImageFly(
                $this->request->param('params'),
                $this->request->param('imagepath')
            );
    }
}
